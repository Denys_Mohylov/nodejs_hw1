const fs = require('fs');
const path = require('path');

const filesDir = './files/';
const extensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];
const passwords = {};

function checkExt (filename){
  return extensions.includes(path.extname(filename).slice(1));
}

function createFile (req, res, next) {
  const filename = req.body.filename;
  const content = req.body.content;

  if (!req.body || !filename || !content) 
    return res.status(400).send({ 'message': "File data is corrupted" });

  if(!checkExt(filename)) 
  return res.status(400).send({ 'message': `File ${filename} has invalid extension ${path.extname(filename).slice(1)}` });

  const filePath = filesDir + filename;

  if (fs.existsSync(filePath)) return res.status(400).send({ 'message': `File ${filename} already exists` });

  fs.writeFileSync(filePath, content,(err) => {
    if (err) return res.status(400).send({'message' : `Failed to save the file ${filename}`});
});

  res.status(200).send({ 'message': `File ${filename} created successfully` });
}

function getFiles (req, res, next) {
    fs.readdir(filesDir, (err, files) => {
      if (err) {
          next({
              message: 'Server error',
              statusCode: 500
          });
          return;
      }

      if (!files.length) 
      return res.status(400).send({"message": "Dir contains no files"})

      return res.status(200).send({
          "message": "Success",
          "files": files
      });
  });
}

const getFile = (req, res, next) => {
  const filename = req.params.filename;
  const filePath = filesDir + filename;

  if (!fs.existsSync(filePath)) {
    return res.status(400).send({ 'message': `No file ${filename} found` });
  }

  const content = fs.readFileSync(filePath).toString();
  const uploadedDate = fs.statSync(filePath).mtime;
  const extension = path.extname(filePath).slice(1);

  return res.status(200).send({
    "message": "Success",
    "filename": filename,
    "content": content,
    "extension": extension,
    "uploadedDate": uploadedDate 
  });
}

function editFile (req, res, next) {
  const filename = req.body.filename;
  const content = req.body.content;
  const filePath = filesDir + filename;
  const currContent = fs.readFileSync(filePath).toString();

  if (!content) 
    return res.status(400).send({ 'message': "File data is corrupted" });
  else if(currContent == content) 
    return res.status(400).send({ 'message': "Nothing to change" });
  
  if (!fs.existsSync(filePath)) return res.status(400).send({ 'message': `File ${filename} does not exists` });

  fs.writeFileSync(filePath, content,(err) => {
    if (err) return res.status(400).send({'message' : `Failed to edit the file ${filename}`});
}, {flag: 'a'});

  res.status(200).send({ 'message': `File ${filename} edited successfully` });
}

const deleteFile = (req, res, next) => {
  const filename = req.params.filename;
  const filePath = filesDir + filename;

  if (!fs.existsSync(filePath)) 
    return res.status(400).send({ 'message': `File ${filename} not found` });

  fs.unlinkSync(filePath);

  return res.status(200).send({ 'message': `File ${filename} deleted successfully` });
}

const createPrivate = (req, res, next) => {
  if (!req.body.password) return next();
  else {
    const filename = req.body.filename;
    const password = req.query.password;
  
    passwords[`${filename}`] = password;
  
    next();
  }
}
const getPrivate = (req, res, next) => {
  const filename = req.params.filename

  if (!passwords[`${filename}`]) return next();

  const password = req.query.password

  if (!password)
    return res.status(400).send({ 'message': 'Password cannot be empty' });

  if (password !== passwords[`${filename}`]) 
    return res.status(403).send({ 'message': 'Password invalid' });

  next();
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile,
  createPrivate,
  getPrivate
}