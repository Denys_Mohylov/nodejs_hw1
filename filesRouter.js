const express = require('express');
const router = express.Router();
const { createFile, getFiles, getFile, editFile, deleteFile, createPrivate, getPrivate } = require('./filesService.js');

router.post('/', createPrivate, createFile);

router.get('/', getFiles);

router.get('/:filename', getPrivate, getFile);

router.put('/', editFile);

router.delete('/:filename', deleteFile);

module.exports = {
  filesRouter: router
};
